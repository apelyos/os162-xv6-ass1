#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

#define NUM_TYPES 3

int stdout = 1;

char *proc_type[] = {"CPU" , "S-CPU" , "IO"};

int main(int argc, char *argv[])
{
  int n = 0, i, pid, j, k;
  int stime = 0, retime = 0, rutime = 0;
  int avstime[NUM_TYPES], avretime[NUM_TYPES], avturntime[NUM_TYPES];
  //char input[100]; //test stime


  // check args
  if (argc < 2) {
    printf(stdout, "Incorrect arg count.\n");
    exit();
  }

  // zero average counts
  for (i=0 ; i < NUM_TYPES ; i++) {
    avstime[i] = 0;
    avretime[i] = 0;
    avturntime[i] = 0;
  }

  n = atoi(argv[1]);

  for (i = 0 ; i < NUM_TYPES*n ; i++) {
    pid = fork();

    if (pid == -1) {
      printf(stdout, "PANIC: fork() didn't like this. Exiting.\n");
      exit();
    }

    // check if child
    if (pid == 0) {
        pid = getpid();

      if (pid % NUM_TYPES == 0) { //type0
      	//printf(stdout, "child type 0, pid:%d\n", pid);

      	for (k = 0 ; k < 100 ; k++) {
      	  for (j = 0 ; j < 1000000 ; j++) {
      	    // dummy
      	  }
      	}
      } else if (pid % NUM_TYPES == 1) { //type1
      	//printf(stdout, "child type 1, pid:%d\n", pid);

      	for (k = 0 ; k < 100 ; k++) {
      	  for (j = 0 ; j < 1000000 ; j++) {
      	    // dummy
      	  }

      	  yield();
      	}
      } else { //type2
      	//printf(stdout, "child type 2, pid:%d\n", pid);
      	//gets(input, 10); //test stime
      	for (j = 0 ; j < 100 ; j++) {
      	  sleep(1);
      	}
      }

        exit();
    }
  }

  // wait for all children to terminate
  printf(stdout, "Waiting for children termination...\n");
  for (i = 0 ; i < NUM_TYPES*n ; i++) {
    pid = wait2(&retime, &rutime, &stime);
    printf(stdout, "pid=%d, type=%s : retime=%d, rutime=%d, stime=%d \n", pid,
      proc_type[pid%NUM_TYPES], retime, rutime, stime);
    avstime[pid % NUM_TYPES] += stime;
    avretime[pid % NUM_TYPES] += retime;
    avturntime[pid % NUM_TYPES] += (stime + retime + rutime);
  }

  printf(stdout, "AVG. stats:\n");
  // calc & print average
  for (i=0 ; i < NUM_TYPES ; i++) {
    avstime[i] /= n;
    avretime[i] /= n;
    avturntime[i] /= n;
    printf(stdout, "type: %s : avg.stime=%d, avg.retime=%d, avg.turntime=%d\n",
      proc_type[i], avstime[i], avretime[i], avturntime[i]);
  }

  printf(stdout, "Bye.\n");

  exit();
}
