#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

#define NUM_TYPES 3
#define NUM_OF_PROC_FOR_PRIO 7

int stdout = 1;

int main(int argc, char *argv[])
{
  int i, pid, j, k;
  int stime = 0, retime = 0, rutime = 0;
  int avstime[NUM_TYPES], avretime[NUM_TYPES], avturntime[NUM_TYPES];

  // zero average counts
  for (i=0 ; i < NUM_TYPES ; ++i) {
    avstime[i] = 0;
    avretime[i] = 0;
    avturntime[i] = 0;
  }

  for (i = 0 ; i < NUM_TYPES*NUM_OF_PROC_FOR_PRIO ; ++i) {
    pid = fork();

    if (pid == -1) {
      printf(stdout, "PANIC: fork() didn't like this. Exiting.\n");
      exit();
    }

    // check if child
    if (pid == 0) {
      pid = getpid();
  		if (set_prio((pid % NUM_TYPES) + 1) != 0)
        printf(stdout, "PANIC: set_prio() didn't like this.\n");

      //printf(stdout, "child type 0, pid:%d\n", pid);
      for (k = 0 ; k < 100 ; k++) {
        for (j = 0 ; j < 1000000 ; j++) {
      	// dummy
        }
      }
      exit();
    }
  }
  // wait for all children to terminate
  printf(stdout, "Waiting for children termination...\n");
  for (i = 0 ; i < NUM_TYPES*NUM_OF_PROC_FOR_PRIO ; i++) {
    pid = wait2(&retime, &rutime, &stime);
    printf(stdout, "pid=%d, priority:%d : retime=%d, rutime=%d, turntime=%d \n", pid,
      pid%NUM_TYPES + 1, retime, rutime, retime+rutime+stime);
    avstime[pid % NUM_TYPES] += stime;
    avretime[pid % NUM_TYPES] += retime;
    avturntime[pid % NUM_TYPES] += (stime + retime + rutime);
  }

  printf(stdout, "AVG. stats:\n");
  // calc & print average
  for (i=0 ; i < NUM_TYPES ; i++) {
    avstime[i] /= NUM_OF_PROC_FOR_PRIO;
    avretime[i] /= NUM_OF_PROC_FOR_PRIO;
    avturntime[i] /= NUM_OF_PROC_FOR_PRIO;
    printf(stdout, "priority:%d : avg.stime=%d, avg.retime=%d, avg.turntime=%d\n",
      i+1, avstime[i], avretime[i], avturntime[i]);
  }

  printf(stdout, "Bye.\n");

  exit();
}
