// Console input and output.
// Input is from the keyboard or serial port.
// Output is written to the screen and serial port.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "traps.h"
#include "spinlock.h"
#include "fs.h"
#include "file.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "x86.h"



#define MAX_HISTORY 16
#define BUF_SIZE 128

char history_array [MAX_HISTORY][BUF_SIZE];
int his_max = 0;

void add_to_history(char *cmd, uint ref){
  int i = 0;
  memmove(history_array[1],history_array[0], sizeof(char)*(MAX_HISTORY - 1)*BUF_SIZE);
  while(1){
    history_array[0][i] = cmd[(i +ref) % BUF_SIZE];
    if(history_array[0][i] == '\n')
      break;
    ++i;
  }
  if(his_max < MAX_HISTORY){
    ++his_max; 
  }
}

int history(char *buffer, int historyId){
 if (historyId >= his_max)
   return -1;
 if (historyId >= MAX_HISTORY || historyId < 0)
   return -2;
 memmove(buffer, history_array[historyId], BUF_SIZE);
 return 0;
}

static void consputc(int);

static int panicked = 0;

static struct {
  struct spinlock lock;
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
    x = -xx;
  else
    x = xx;

  i = 0;
  do{
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
    consputc(buf[i]);
}
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
  if(locking)
    acquire(&cons.lock);

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
    if(c != '%'){
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
    case 'd':
      printint(*argp++, 10, 1);
      break;
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
        consputc(*s);
      break;
    case '%':
      consputc('%');
      break;
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
      consputc(c);
      break;
    }
  }

  if(locking)
    release(&cons.lock);
}

void
panic(char *s)
{
  int i;
  uint pcs[10];
  
  cli();
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
  for(;;)
    ;
}

//PAGEBREAK: 50
#define BACKSPACE 0x100
#define ARR_LEFT 228
#define ARR_RIGHT 229
#define CRTPORT 0x3d4
#define ARR_UP 226
#define ARR_DOWN 227
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
  pos = inb(CRTPORT+1) << 8;
  outb(CRTPORT, 15);
  pos |= inb(CRTPORT+1);

  if(c == '\n')
    pos += 80 - pos%80;
  else if(c == BACKSPACE){
    if(pos > 0) {
      --pos;
      memmove(crt+pos, crt+pos+1, sizeof(crt[0])*128); // new: shift screen left
    }
  } else if (c == ARR_LEFT) {
      pos--;
  } else if (c == ARR_RIGHT) {
      pos++;
  } else {
    memmove(crt+pos+1, crt+pos, sizeof(crt[0])*128); // new: shift screen right
    
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
  }

  if(pos < 0 || pos > 25*80)
    panic("pos under/overflow");
  
  if((pos/80) >= 24){  // Scroll up.
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
    pos -= 80;
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
  }
  
  outb(CRTPORT, 14);
  outb(CRTPORT+1, pos>>8);
  outb(CRTPORT, 15);
  outb(CRTPORT+1, pos);
  //crt[pos] = ' ' | 0x0700;
}

void
consputc(int c)
{
  if(panicked){
    cli();
    for(;;)
      ;
  }

  if(c == BACKSPACE){
    uartputc('\b'); uartputc(' '); uartputc('\b');
  } else
    uartputc(c);
  cgaputc(c);
}

#define INPUT_BUF 128
struct {
  char buf[INPUT_BUF];
  uint r;  // Read index
  uint w;  // Write index
  uint e;  // Edit index
  uint m;  // max index **added**
} input;

#define C(x)  ((x)-'@')  // Control-x
int his_index = -1;

void
consoleintr(int (*getc)(void))
{
  int c, doprocdump = 0;
  uint i = 0;
  int ret;
  char buffer[128];

  acquire(&cons.lock);
  while((c = getc()) >= 0){
    switch(c){
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
	input.m--;
        consputc(BACKSPACE);
      }
      break;
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
	input.e--;
	input.m--; // maintain max
	
	for (i = input.e ; i < input.m ; i++) { // new: shift right-hand buffer left
	  input.buf[i % INPUT_BUF] = input.buf[(i+1) % INPUT_BUF];
	}

        consputc(BACKSPACE);
      }
      break;
    /* new code */
    case ARR_LEFT:
      if(input.e > input.r) {
	input.e--;
	consputc(ARR_LEFT);
      }
      break;
    case ARR_RIGHT:
      if (input.e < input.m) {
	input.e++;
	consputc(ARR_RIGHT);
      }
      break;
    case ARR_UP:
      ++his_index;
      ret = history(buffer, his_index);
      if(ret != 0){
	--his_index;
	break;
      }
      input.e = input.m;
      while(input.e != input.w &&input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.m--;
        input.e--;
        consputc(BACKSPACE);
      }
      i = 0;
      while(buffer[i] != '\n'){
	input.buf[(i+input.r) % INPUT_BUF] = buffer[i];
	input.m++;
	input.e++;
	consputc(buffer[i]);
	++i;
      }

      break;
    case ARR_DOWN:
      --his_index;
      ret = history(buffer, his_index);
      if(ret != 0){
	++his_index;
	break;
      }
      input.e = input.m;
      while(input.e != input.w &&input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.m--;
        input.e--;
        consputc(BACKSPACE);
      }
      i = 0;
      while(buffer[i] != '\n'){
	input.buf[(i+input.r) % INPUT_BUF] = buffer[i];
	input.m++;
	input.e++;
	consputc(buffer[i]);
	++i;
      }

      break;      
      /* end of new code */
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
        c = (c == '\r') ? '\n' : c;
	
        consputc(c);
	
        if(c == '\n' || c == C('D') || input.m == input.r+INPUT_BUF){
	  input.buf[input.m++ % INPUT_BUF] = c; //moved
	  if (c == '\n'){
 	    if (input.r != (input.m -1))
	      add_to_history(input.buf, input.r);
	    his_index = -1;
	  }
	  input.e = input.m; // new
          input.w = input.m; 
          wakeup(&input.r);
        } else {
	  input.m++; // maintain max
	  
	  for (i = input.m+1 ; i > input.e && i > 0 ; i--) { // new: shift right-hand buffer right
	    input.buf[i % INPUT_BUF] = input.buf[(i-1) % INPUT_BUF];
	  }
	  input.buf[input.e++ % INPUT_BUF] = c;
	}
      }
      break;
    }
  }
  release(&cons.lock);
  if(doprocdump) {
    procdump();  // now call procdump() wo. cons.lock held
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
  uint target;
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
      if(proc->killed){
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
    if(c == C('D')){  // EOF
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}

int
consolewrite(struct inode *ip, char *buf, int n)
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
    consputc(buf[i] & 0xff);
  release(&cons.lock);
  ilock(ip);

  return n;
}

void
consoleinit(void)
{
  initlock(&cons.lock, "console");

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  picenable(IRQ_KBD);
  ioapicenable(IRQ_KBD, 0);
}

